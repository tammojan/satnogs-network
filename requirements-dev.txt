# This is a generated file; DO NOT EDIT!
#
# Please edit 'setup.cfg' to add top-level extra dependencies and use
# './contrib/refresh-requirements.sh to regenerate this file
-r requirements.txt

apipkg==1.5
atomicwrites==1.3.0
attrs==19.3.0
coverage==5.0
execnet==1.7.1
factory-boy==2.11.1
Faker==0.8.18
filelock==3.0.12
funcsigs==1.0.2
ipaddress==1.0.23
mock==2.0.0
packaging==19.2
pbr==5.4.4
pluggy==0.13.1
pur==5.2.2
py==1.8.0
pyparsing==2.4.5
pytest==4.6.7
pytest-cov==2.6.1
pytest-django==3.4.8
pytest-forked==0.2
pytest-xdist==1.25.0
python-dateutil==2.8.1
text-unidecode==1.2
toml==0.10.0
tox==3.14.2
virtualenv==16.7.9
wcwidth==0.1.7
